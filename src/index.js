import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from './App';
import reportWebVitals from "./reportWebVitals";
import AppHeader from "./Components/Header/Header";
import AppFooter from "./Components/Footer/Footer";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Page/Home/HomePage";
import ContactPage from "./Page/Contact/ContactPage";
import App from "./App";
import TestPage from "./Page/Test/TestPage";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <AppHeader isLoggedIn={true} />
    <BrowserRouter>
      <Routes>
        <Route path="/app" element={<App />} />

        <Route path="/" element={<HomePage />} />
        <Route path="/contact" element={<ContactPage />} />
        <Route path="/test" element={<TestPage />} />
      </Routes>
    </BrowserRouter>
    <AppFooter />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
