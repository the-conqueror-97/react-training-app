import React from "react";

export default class TestPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.test();
  }

  test() {
    let user = {
      taille: "1.83m",
      name: "Fatih"
    };

    console.log("user", user, user.name);



    let users = [
      {
        taille: "1.83m",
        name: "Fatih",
        company: {
          companyName: "webinify",
          employees: []
        }
      }, {
        taille: "1.58m",
        name: "Rabia",
        company: {
          companyName: "webinify",
          employees: []
        }
      }
    ];
    console.log("users", users, users[0]);


    

    let company = {
      companyName: "webinify",
      employees: users
    };

    console.log("entreprise", company, company.employees);

    users.forEach(user => {
      console.log("___user", user);
    });

    company.employees.forEach(employee => {
      console.log("___employee", employee);
    });

    this.testMap();
  }

  testMap() {
    let users = [
      {
        taille: "1.83m",
        name: "Fatih",
        company: {
          companyName: "webinify",
          employees: []
        }
      }, {
        taille: "1.58m",
        name: "Rabia",
        company: {
          companyName: "webinify",
          employees: []
        }
      }
    ];

    users = users.map(user => user.company);

    // users = users.map((user) => {
    //     return user.company.companyName;
    // });

    console.log("users", users);
  }

  render() {
    return (< div className = "TestPage" > <div > <h3 > Test page < /h3> <
            p > lorem ipsum < /p > < /div> <
            div > < /div > < /div>
        );
    }
}