import React from "react";


export default class FooterMenu extends React.Component {


    BuildMenuElements(props) {
        const numbers = 5;
        let listItems = [];
        const keyPrefix = Math.floor(Math.random() * 100);
    
        for (let i = 0; i < numbers; i++) {
         
          listItems.push(
            <li key={keyPrefix + "-" + i}>
              <a href="/">Footer Link {keyPrefix + "-" + i}</a>
            </li>
          );
        }
    
        return <ul>{listItems}</ul>;
      }

    render() {
        const numbers = 5;
        let listItems = [];
    
        for (let i = 0; i < numbers; i++) {
          listItems.push(
           <this.BuildMenuElements />
          );
        }
    
        return <div className="FooterMenu">{listItems}</div>;
    }
}