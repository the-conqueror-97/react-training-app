import React from "react";
import "./AppHeader.scss";

/**
 * This is going to render only header
 * You cant have two render like functions
 */
export default class AppHeader extends React.Component {
  constructor(props) {
    super(props);

    /**
     * Bind functions in variables
     */
    //this.toggleHeader = this.toggleHeader.bind(this);
    this.toggleLogin = this.toggleLogin.bind(this);

    /**
     * Init state
     */
    this.state = {
      isSuperHeaderVisible: this.props.isSuperHeaderVisible || false,
      isLoggedIn: this.props.isLoggedIn || false,
    };
  }

  componentDidMount() {}

  /**
   * This will simply toggle header
   */
  toggleHeader() {
    console.log(this.props);

    // console.log("toggle", this.state.isSuperHeaderVisible);
    // this.setState({
    //   isSuperHeaderVisible: !this.state.isSuperHeaderVisible,
    // });
  }

  componentWillUnmount() {}

  toggleLogin() {
    this.setState({
      isLoggedIn: !this.state.isLoggedIn,
    });
  }

  /**
   * Cela empeche de mettre a jours ce composent si on return false
   * 
   * @returns 
   */
  shouldComponentUpdate() {
    return true;
  }

  // /**
  //  * WARNING!: This doesnt work,
  //  * You must separate in different class
  //  * You cant call this in other methods, a separate class MenuLink that extends React component is required
  //  * Just think like FLutter
  //  *
  //  * @param {*} props
  //  * @returns
  //  */
  // MenuLink(props) {
  //   return <li className="MenuLink" key={props.key}></li>;
  // }

  BuildMenuElements(props) {
    console.log(props);
    const routes = [
      {
        title: "home",
        url: "/",
      },
      {
        title: "contact",
        url: "/contact",
      },
    ];
    let listItems = [];
    const keyPrefix = Math.floor(Math.random() * 100);

    routes.forEach((route, i) => {
      listItems.push(
        <li key={keyPrefix + "-" + i}>
          <a href={route.url}> {route.title}</a>
        </li>
      );
    });

    // // You cant do this
    // // You must use a separate
    // listItems.push( <this.MenuLink/>);

    // for (let i = 0; i < routes.length; i++) {
    //   listItems.push(
    //     <li key={keyPrefix + "-" + i}>
    //       <a href="/"> Link {keyPrefix + "-" + i} </a>
    //     </li>
    //   );
    // }

    // listItems.push(
    //   <li key={numbers}>
    //     <button className="LoginButton" onClick={() => this.toggleLogin()}>
    //       {this.state.isLoggedIn ? "My account" : "Se connecter"}
    //     </button>
    //   </li>
    // );

    return <ul>{listItems}</ul>;
  }
  render() {
    const data = {
      apple: 1,
      mangol: 2
    };

    return (
      <header className="AppHeader">
        <div className="AppLogo">
          <img src="/" alt="app logo"></img>
        </div>
        <div className="HeaderMenu">
          <this.BuildMenuElements {...data}/>

          <button className="LoginButton" onClick={this.toggleHeader}>
            {this.state.isLoggedIn ? "My account" : "Se connecter"}
          </button>
        </div>
      </header>
    );
  }
}
